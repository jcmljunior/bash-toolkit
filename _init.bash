#!/usr/bin/env bash

source "toolkit.bash"

[ -n "$1" ] && [[ "$(function_exists "$1")" ]] && {
	eval "$1 ${@:2}"
}

