#!/usr/bin/env bash

# TIPAGEM DE ELEMENTOS.
# PROJETO: (https://gitlab.com/jcmljunior/bash-toolkit)
# AUTOR: JULIO CESAR <jcmljunior@gmail.com>
# VERSÃO: 1.0.0

# CONSULTA $1 PARA SABER SE O VALOR É DO TIPO STRING.
# EX: declare -- STRING="BLABLABLA123" | is_string "$STRING"
function is_string()
{
  [[ "$(declare -p 2>/dev/null)" =~ "declare --" ]] && {
		echo "STRING"
  }
}

# CONSULTA $1 PARA SABER SE O VALOR É DO TIPO NÚMERICO.
# EX: declare -- NUMBER="123" | is_number "$NUMBER"
function is_number()
{
  [[ "$(declare -p 2>/dev/null)" =~ "declare --" ]] && [[ "$1" == ?(-)+([0-9]) ]] && {
		echo "NUMBER"
  }
}

# CONSULTA $1 PARA SABER SE O VALOR É DO TIPO FLOAT.
# declare -- FLOAT="125.25" | is_float "$FLOAT"
function is_float()
{
  [[ "$(declare -p 2> /dev/null)" =~ "declare --" ]] && [ -n "$(echo "$1" | grep -oE "^[0-9]+[,.]")" ] && {
		echo "FLOAT"
  }
}

# CONSULTA $1 PARA SABER SE O VALOR É DO TIPO ARRAY.
# EX: declare -a ARR=(valor1) | is_array "$ARR"
function is_array()
{
  [[ "$(declare -p 2>/dev/null)" =~ "declare -a" ]] && {
		echo "ARRAY"
  }
}

# CONSULTA $1 PARA SABER SE O VALOR É DO TIPO MAP.
# EX: declare -A ARR=([key]=value) | is_map $ARR
function is_map()
{
  [[ "$(declare -p 2>/dev/null)" =~ "declare -A" ]] && {
		echo "MAP"
  }
}

# CONSULTA $1 PARA SABER SE O VALOR É UMA FUNCTION.
# EX: function_exists "is_map"
function function_exists()
{
  [[ -n "$(is_string "$1")" ]] && [[ "$(declare -F | grep -oE "(declare -f "$1")")" ]] && {
		echo "FUNCTION"
  }
}

